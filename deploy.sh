#!/bin/sh

git push
npm run build
chmod g+r dist/* public/*
rsync --progress --update -avz --delete \
  dist/* root@139.180.170.17:/var/www/mr-barber
rsync --progress --update -avz --delete \
  public/* root@139.180.170.17:/var/www/mr-barber
