# Installation

[Node.js] is required for installation.

```shell
npm install
```

# Running locally

This project uses [React Scripts](https://create-react-app.dev/docs/getting-started#scripts) to build and serve.

```
npm start
```

## Building for Production

```
npm run build
```

## Testing

```
npm test
```

# Deploying the Website

```
./deploy-build.sh
```

# Deploying caddy

```
./deploy-caddy.sh
```
