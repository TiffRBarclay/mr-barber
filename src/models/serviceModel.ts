export interface ServiceModel {
  id: number;
  name: string;
  price: number;
}
