export interface BarberModel {
  name: string;
  id: number;
  appointmentDays: string[];
  active: boolean;
}
