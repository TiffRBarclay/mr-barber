export interface ShopModel {
  id: number;
  name: string;
  address: string;
  allowWalkIns: boolean;
}
