export interface EventModel {
  id?: number;
  phone: string;
  email: string;
  name: string;
  start: Date;
  shopId: number;
  serviceId: number;
  barberId?: number;
  service?: { name: string };
  barber?: { name: string };
}
