import React from 'react';
import { render, screen } from '@testing-library/react';
import { Home } from './Home';

test('Renders Section Title', () => {
  render(<Home />);
  expect(
    screen.getAllByText(/MR\. BARBER - THE BARBER SCHOOL/).length,
  ).toBeGreaterThan(0);
});
