import Contact from '../components/Contact';
import { ContentWrapper } from '../components/ContentWrapper';
import { PageHeader } from '../components/PageHeader';
import { SectionHeader } from '../components/SectionHeader';
import headerImage from '../images/barber-14.webp';
import scissors from '../images/scissors.webp';
import { TableContainer } from './Bookings';

export const Shops = () => {
  return (
    // <ShopWrapper>
    <>
      <PageHeader
        title="OUR SHOPS"
        subTitle="WE TAKE WALK-INS"
        icon={scissors}
        backgroundImage={headerImage}
      />
      <ContentWrapper>
        <div id="training-centre">
          <SectionHeader title="TRAINING CENTRE" />
        </div>
        <TableContainer style={{ marginBottom: 20 }}>
          <Contact email="mrbarber@mrbarber.co.nz">
            <tr>
              <td>Open hours</td>
              <td>
                <p>10am - 12pm</p> <p>1pm - 3:30 pm</p>
              </td>
            </tr>
            <tr>
              <td>Address</td>
              <td><a href="https://maps.app.goo.gl/kDfj5igzfX9wefiW6">107 Albert Street, Auckland City</a></td>
            </tr>
          </Contact>
        </TableContainer>
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3192.719582430429!2d174.76039377584058!3d-36.84919137223328!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d0d47faa83f7241%3A0x5701a09b84452437!2s107%20Albert%20Street%2C%20Auckland%20CBD%2C%20Auckland%201010!5e0!3m2!1sen!2snz!4v1724119586074!5m2!1sen!2snz"
            width="600" height="450"  style={{border: 0, width: '100%'}} allowFullScreen
            loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>

        <div id="epsom-shop">
          <SectionHeader title="EPSOM STORE" />
        </div>
        <TableContainer style={{ marginBottom: 20 }}>
          <Contact email="mrbarber@mrbarber.co.nz">
            <tr>
              <td>Open hours</td>
              <td>
                <p>Monday - Friday: 9am - 6pm</p>
                <p>Saturday: 9am - 4pm</p>
                <p>Sunday: 9am - 3pm</p>
              </td>
            </tr>
            <tr>
              <td>Address</td>
              <td><a href="https://maps.app.goo.gl/VKnCzhW6tLYkUw5D8">604 Manukau Road Epsom, Auckland</a></td>
            </tr>
          </Contact>
        </TableContainer>
        <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3190.577474012249!2d174.77093327584294!3d-36.90045507221864!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6d0d489e2636a807%3A0x7576b6745d64f25b!2s604%20Manukau%20Road%2C%20Epsom%2C%20Auckland%201023!5e0!3m2!1sen!2snz!4v1724119840764!5m2!1sen!2snz"
            width="600" height="450"  style={{border: 0, width: '100%'}} allowFullScreen
            loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>

      </ContentWrapper>
    </>
  );
};
