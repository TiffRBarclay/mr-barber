import { useMemo, useState } from "react";
import { BarberModel } from "../models/barberModel";
import { EventModel } from "../models/eventModel";
import { ServiceModel } from "../models/serviceModel";
import { FormDropdown } from "../pages/Bookings";
import { createEvent, sendEmail, updateEvent } from "../services/event.service";
import { graphql } from "../services/graphql.service";
import { EventForm, FormInput } from "./EventForm";
import Modal from "./Modal";

export default function EditEvent({
  onClose,
  event,
  services,
  barbers,
  update,
  showSnack,
  shopId,
  events,
}: {
  onClose: () => void;
  event: EventModel;
  services: ServiceModel[];
  barbers: BarberModel[];
  update: () => Promise<void>;
  showSnack: (message: string) => void;
  shopId: number;
  events: EventModel[];
}) {
  const [eventDetails, setEventDetails] = useState<EventModel>(event);
  const [submitting, setSubmitting] = useState(false);

  const getShop = async (id: number): Promise<string> => {
    const query = `
      query Get($id: Int) {
        shop(where: {id: {_eq: $id}}) {
          name
        }
      }
    `;
    const { data } = await graphql(query, { id });
    return data.shop[0].name;
  };

  const getService = async (id: number): Promise<string> => {
    const query = `
      query Get($id: Int) {
        service(where: {id: {_eq: $id}}) {
          name
        }
      }
    `;
    const { data } = await graphql(query, { id });
    return data.service[0].name;
  };

  const handleAppointment = async () => {
    const tzOffset = eventDetails.start.getTimezoneOffset() * 60000;
    console.log({ tzOffset });
    const msLocal = eventDetails.start.getTime() - tzOffset;
    const dateLocal = new Date(msLocal);
    const iso = dateLocal.toISOString().slice(0, -1);
    const updatedEvent = {
      ...eventDetails,
      start: iso,
    };
    setSubmitting(true);
    try {
      const res = eventDetails.id
        ? await updateEvent(updatedEvent as EventModel & { start: string })
        : await createEvent(updatedEvent as EventModel & { start: string });
      if (res?.errors?.[0]?.message?.includes("Uniqueness")) {
        showSnack(
          "You have already made an appointment. We look forward to seeing you.",
        );
        onClose();
        return;
      }
      await sendEmail({
        email: updatedEvent.email,
        service: updatedEvent.serviceId
          ? await getService(updatedEvent.serviceId)
          : "Standard cut",
        start: updatedEvent.start,
        shop: await getShop(shopId),
      });
      await update();
      showSnack(
        "Appointment made successfully. We look forward to seeing you.",
      );
      onClose();
    } finally {
      setSubmitting(false);
    }
  };

  const remove = async () => {
    const sure = confirm(
      `Are you sure you want to delete appointment for ${eventDetails.name}`,
    );
    if (!sure) return;
    setSubmitting(true);
    try {
      const query = `
      mutation Delete($id: Int!){
        delete_event_by_pk(id: $id) {
          id
        }
      }
    `;
      const { errors } = await graphql(query, { id: eventDetails.id });
      if (errors) showSnack(errors[0].message);
      await update();
      onClose();
    } finally {
      setSubmitting(false);
    }
  };

  const { start } = eventDetails;
  const weekday = start.toLocaleString("en-nz", {
    weekday: "short",
  });

  const processedServices =
    weekday === "Sat" || weekday === "Sun" || start.getHours() >= 15
      ? services.filter(({ name }) => !name.includes("Seniors"))
      : services;

  const processedBarbers = barbers.filter((barber) => {
    const clash = events.find(
      (e) =>
        e.start.getTime() === start.getTime() &&
        e.barberId === barber.id &&
        e.shopId === shopId &&
        e.id !== event.id,
    );
    return barber.appointmentDays.some((day) => day.startsWith(weekday)) &&
      !clash;
  });

  const submitDisabled = useMemo(
    () => submitting || processedBarbers.length === 0,
    [processedBarbers, submitting],
  );

  return (
    <Modal title="New Appointment" onClose={onClose}>
      <EventForm
        onSubmit={(event) => {
          event.preventDefault();
          handleAppointment();
        }}
      >
        <label>
          <span style={{ width: 60 }}>Date:</span>
          {`${start.getDate()} ${
            start.toLocaleString("en-nz", {
              weekday: "short",
            })
          }, ${
            start.getHours() > 12 ? start.getHours() - 12 : start.getHours()
          }:${start.getMinutes() === 0 ? "00" : start.getMinutes()} ${
            start.getHours() >= 12 ? "PM" : "AM"
          }`}
        </label>
        <label>
          <span>Name</span>
          <FormInput
            required
            value={eventDetails.name}
            onChange={(event) =>
              setEventDetails({ ...eventDetails, name: event.target.value })}
            autoFocus
          />
        </label>
        <label>
          <span>Phone</span>
          <FormInput
            type="tel"
            value={eventDetails.phone}
            onChange={(event) =>
              setEventDetails({ ...eventDetails, phone: event.target.value })}
          />
        </label>
        <label>
          <span>Email</span>
          <FormInput
            type="email"
            value={eventDetails.email}
            onChange={(event) =>
              setEventDetails({ ...eventDetails, email: event.target.value })}
          />
        </label>
        <label>
          <span>Service</span>
          <FormDropdown
            style={{ width: "100%" }}
            required
            value={eventDetails.serviceId}
            onChange={(event) =>
              setEventDetails({
                ...eventDetails,
                serviceId: Number(event.target.value),
              })}
          >
            {processedServices.map((service) => (
              <option key={`new-event-${service.name}`} value={service.id}>
                {service.name}
              </option>
            ))}
          </FormDropdown>
        </label>
        <label>
          <span>Barber</span>
          <FormDropdown
            style={{ width: "100%" }}
            required
            value={eventDetails.barberId || 0}
            onChange={(event) =>
              setEventDetails({
                ...eventDetails,
                barberId: Number(event.target.value),
              })}
          >
            <option value={0}>Any</option>
            {processedBarbers.map((barber) => (
              <option key={`new-event-${barber.name}`} value={barber.id}>
                {barber.name}
              </option>
            ))}
          </FormDropdown>
        </label>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <button onClick={onClose} type="button">
            CANCEL
          </button>
          {localStorage.getItem("password") && eventDetails.id && (
            <button
              type="button"
              onClick={remove}
              style={{ background: "#645875" }}
            >
              DELETE
            </button>
          )}
          {processedBarbers.length === 0 && <span>No barbers available.</span>}
          <button
            disabled={submitDisabled}
            style={{
              background: submitDisabled ? "lightgrey" : "",
              cursor: submitDisabled ? "disabled" : "",
            }}
          >
            {submitting ? "SUBMITTING..." : "SUBMIT"}
          </button>
        </div>
      </EventForm>
    </Modal>
  );
}
