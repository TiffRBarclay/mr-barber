import styled from 'styled-components';

export const Table = styled.table`
  td,
  th {
    border: 1px solid #ddd;
    padding: 8px;
  }

  table {
    width: 100%;
  }

  th {
    font-size: 26px;
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: rgba(10, 10, 10, 0.76);
    color: white;
  }

  ul {
    text-align: start;
  }
`;

export default function Contact({
  children,
  email,
  phone,
  phoneText,
  mobile,
  mobileText,
  website,
}: {
  children?: React.ReactElement<any, any> | React.ReactElement<any, any>[];
  email: string;
  phone?: string;
  phoneText?: string;
  mobile?: string;
  mobileText?: string;
  website?: string;
}) {
  return (
    <Table>
      <tbody>
        <tr>
          <td>Contact</td>
          <td>
            <a href={`mailto:${email}`}>{email}</a>
          </td>
        </tr>
        {website ? (
          <tr>
            <td>Website</td>
            <td>
              <a href={`https://${website}`}>{website}</a>
            </td>
          </tr>
        ) : (
          <tr>
            <td>Phone</td>
            <td>
              <a href={`tel:${phone || '099291150'}`}>
                {phoneText || '(09) 929-1150 '}
              </a>
            </td>
          </tr>
        )}
        <tr>
          <td>Mobile</td>
          <td>
            <a href={`tel:${mobile || '0210440708'}`}>
              {mobileText || '021-044-0708'}
            </a>
          </td>
        </tr>
        {children}
      </tbody>
    </Table>
  );
}
