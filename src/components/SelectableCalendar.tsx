import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { getDayIntervals, getTimeIntervals } from "../hooks/dates";
import { BarberModel } from "../models/barberModel";
import { EventModel } from "../models/eventModel";
import { ServiceModel } from "../models/serviceModel";
import { StoreForm } from "../pages/Bookings";
import { getEvents } from "../services/event.service";
import EditEvent from "./EditEvent";
import { FormInput } from "./EventForm";
import Row from "./Row";
import { LOGIN_EVENT } from "./SignIn";
import Snackbar from "./Snackbar";

const Table = styled.table`
  width: 100%;
  border-spacing: 0;
  height: 75vh;
  th,
  td {
    border: 1px solid #ddd;
    min-width: 100px;
    &:first-child {
      width: 80px;
    }
  }
  tbody > tr {
    height: 3em;
  }
`;

const CalendarContainer = styled.div`
  overflow-x: auto;

  .rbc-calendar {
    width: 100%;
  }
`;

export default function SelectableCalendar({
  shop,
  services,
  barbers,
}: {
  shop: number;
  services: ServiceModel[];
  barbers: BarberModel[];
}) {
  const [events, setEvents] = useState<EventModel[]>([]);
  const [day, setDay] = useState<string>(
    new Date().toISOString().substr(0, 10),
  );
  const [shownEvent, setShownEvent] = useState<EventModel>();
  const [message, setMessage] = useState("");
  const [show, setShow] = useState(false);

  const updateEvents = async () => {
    const newEvents = await getEvents(shop);
    setEvents(
      newEvents.map((event) => {
        return { ...event, start: event.start };
      }),
    );
  };

  const showSnack = (message: string) => {
    setMessage(message);
    setShow(true);
    setTimeout(() => setShow(false), 5000);
  };

  useEffect(() => {
    window.addEventListener(LOGIN_EVENT, async () => {
      await updateEvents();
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    updateEvents().then();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [day, shop]);

  const getInterval = () => {
    // eslint-disable-next-line no-restricted-globals
    const screenWidth = screen.width;
    if (screenWidth <= 320) return 1;
    if (screenWidth <= 600) return 3;
    return 7;
  };

  const week = getDayIntervals(new Date(day), getInterval());
  const timeIntervals = getTimeIntervals();

  return (
    <>
      {shownEvent && (
        <EditEvent
          shopId={shop}
          onClose={() => setShownEvent(undefined)}
          event={shownEvent}
          services={services}
          barbers={barbers}
          update={updateEvents}
          showSnack={showSnack}
          events={events}
        />
      )}
      <div style={{ display: "flex", flexDirection: "column", width: "100%" }}>
        <StoreForm onSubmit={(e) => e.preventDefault()}>
          <label>
            <span style={{ width: 115, marginRight: 0 }}>Select a date:</span>
            <div style={{ display: "flex" }}>
              <button
                onClick={() => {
                  const newDate = new Date(day);
                  setDay(
                    new Date(newDate.setDate(newDate.getDate() - getInterval()))
                      .toISOString()
                      .substr(0, 10),
                  );
                }}
              >
                <FontAwesomeIcon
                  style={{ marginLeft: 5 }}
                  icon={faChevronLeft}
                />
              </button>
              <FormInput
                style={{ width: 200 }}
                type="date"
                value={day}
                onChange={(e) => {
                  setDay(e.target.value);
                }}
              />
              <button
                onClick={() => {
                  const newDate = new Date(day);
                  setDay(
                    new Date(
                      newDate.setDate(newDate.getDate() + getInterval() + 1),
                    )
                      .toISOString()
                      .substr(0, 10),
                  );
                }}
              >
                <FontAwesomeIcon
                  style={{ marginLeft: 5 }}
                  icon={faChevronRight}
                />
              </button>
            </div>
          </label>
        </StoreForm>

        <CalendarContainer>
          <Table>
            <thead>
              <tr>
                <th />
                {week.map((day) => (
                  <th
                    key={day.getTime()}
                  >
                    {`${day.getDate()} ${
                      day.toLocaleDateString("en-nz", {
                        weekday: "short",
                      })
                    }`}
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {timeIntervals.map((interval) => (
                <Row
                  key={interval.toISOString()}
                  week={week}
                  events={events}
                  setShownEvent={setShownEvent}
                  shopId={shop}
                  shownEvent={shownEvent}
                  interval={interval}
                />
              ))}
            </tbody>
          </Table>
        </CalendarContainer>
      </div>
      {show && <Snackbar message={message} />}
    </>
  );
}
