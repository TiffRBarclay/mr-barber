import { PUBLIC_HOLIDAYS } from '../constants/publicHolidays';
import { EventModel } from '../models/eventModel';
import styles from './Row.module.css';

const TODAY = new Date();

export default function Row({
  interval,
  week,
  events,
  shownEvent,
  setShownEvent,
  shopId,
}: {
  interval: Date;
  week: Date[];
  events: EventModel[];
  shownEvent: EventModel | undefined;
  setShownEvent: (event: EventModel) => void;
  shopId: number;
}) {
  const defaultEvent = {
    name: '',
    phone: '',
    start: 0,
    shopId,
    serviceId: 1,
    email: '',
  };
  return (
    <tr key={interval.getTime()}>
      <td>
        {`${
          interval.getHours() > 12
            ? interval.getHours() - 12
            : interval.getHours()
        }:${interval.getMinutes() === 0 ? '00' : interval.getMinutes()} ${
          interval.getHours() <= 12 ? 'AM' : 'PM'
        }`}
      </td>
      {week.map((day) => {
        const dayAndInterval = new Date(day);
        dayAndInterval.setHours(interval.getHours());
        dayAndInterval.setMinutes(interval.getMinutes());

        const weekday = dayAndInterval.toLocaleDateString('en-nz', {
          weekday: 'short',
        });
        const appointments = events.filter(
          (event) => event.start.getTime() === dayAndInterval.getTime(),
        );
        const publicHoliday = PUBLIC_HOLIDAYS.some((holiday) => {
          const compareDay = new Date(day);
          compareDay.setHours(0);
          compareDay.setMinutes(0);
          return holiday.getTime() === compareDay.getTime();
        });
        const isDisabled =
          TODAY > dayAndInterval ||
          (weekday === 'Sat' && dayAndInterval.getHours() >= 15) ||
          (weekday === 'Sun' && dayAndInterval.getHours() >= 14) ||
          dayAndInterval.getHours() >= 17 ||
          appointments.length >= 3 ||
          publicHoliday ||
          appointments.length >= 2;

        const clickSlot = () => {
          if (isDisabled || shownEvent) return;
          setShownEvent({
            ...defaultEvent,
            start: dayAndInterval,
          } as EventModel);
        };

        return (
          <td
            key={day.toString()}
            onClick={clickSlot}
            style={{
              cursor: isDisabled ? 'not-allowed' : 'pointer',
              background: isDisabled ? 'lightgrey' : 'white',
            }}
          >
            {localStorage.getItem('password') &&
              appointments.map((appointment) => (
                <div
                  key={appointment.id}
                  style={{ cursor: 'pointer' }}
                  onClick={(e) => {
                    e.stopPropagation();
                    setShownEvent(appointment);
                  }}
                  className={styles.event}
                >
                  <p className={styles.title}>
                    {appointment.name}{' '}
                    {appointment.phone ? `(${appointment.phone})` : null}
                  </p>
                  <em>
                    {appointment.barber?.name || 'Any barber'} -{' '}
                    {appointment.service?.name}
                  </em>
                </div>
              ))}
          </td>
        );
      })}
    </tr>
  );
}
