import { EventModel } from '../models/eventModel';
import { graphql } from './graphql.service';

export const isAuthenticated = async () => {
  const query = `#graphql
    query GetPhone {
      event(limit:1) {
        phone
      }
    }
  `;
  const res = await graphql(query, {});
  return !!res?.data?.event;
};

export const getEvent = async (id: number) => {
  const query = `#graphql
    query GetEvent($id: Int!) {
      event(where: {id: {_eq: $id}}) {
        id
        name
        phone
        start
      }
    }
  `;
  const res = await graphql(query, { id });
  return res.data.event[0];
};

export const getEvents = async (shopId: number): Promise<EventModel[]> => {
  const after = new Date();
  after.setDate(after.getDate() - after.getDay());
  const hasPassword = !!localStorage.getItem('password');
  const query = `#graphql
    query GetEvents($after: timestamp!, $shopId: Int) {
      event(where: {start: {_gt: $after}, shopId: {_eq: $shopId}}) {
        id
        start
        shopId
        barberId
        ${hasPassword ? 'serviceId phone name email' : ''}
        service { name }
        barber { name }
      }
    }
`;
  const res = await graphql(query, { after, shopId });
  const updatedData: EventModel[] = res.data.event.map((item: any) => ({
    ...item,
    start: new Date(item.start),
  }));
  return updatedData;
};

export const updateEvent = async (
  event: EventModel,
): Promise<{ errors?: any; id?: number }> => {
  delete event.barber;
  delete event.service;
  const query = `#graphql
    mutation InsertEvent($event: event_insert_input!){
      insert_event_one(object:$event, on_conflict: {
        constraint:event_pkey, 
        update_columns: [barberId email name phone start serviceId shopId]
      }) {
        id
      }
    }
  `;
  return graphql(query, { event });
};

export const createEvent = async (event: EventModel): Promise<any> => {
  const query = `#graphql
    mutation CreateEvent($event: event_insert_input!) {
      insert_event_one(object: $event) {
        id
      }
    }
`;
  return graphql(query, { event });
};

export const sendEmail = async ({
  email,
  service,
  start,
  shop,
}: {
  email: string;
  service: string;
  start: string;
  shop: string;
}): Promise<void> => {
  const query = `
    mutation Mail($email: String, $service: String, $start: String, $shop: String) {
      sendEmail(
        email: $email, 
        service: $service, 
        start: $start,
        shop: $shop
      )
    }
  `;
  await graphql(query, {
    email,
    service,
    start,
    shop,
  });
};
