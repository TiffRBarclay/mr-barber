export const SERVICES: { name: string; price: number }[] = [
  { name: 'Haircut', price: 35 },
  { name: 'Skin fade', price: 45 },
  { name: 'Clipper cut', price: 25 },
  { name: 'Kids under 16', price: 25 },
  { name: 'Beards from', price: 15 },
  { name: 'Sampoo', price: 10 },
  { name: 'Dad and child', price: 50 },
  { name: 'Seniors*', price: 25 },
];
