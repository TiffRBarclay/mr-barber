export const PUBLIC_HOLIDAYS: Date[] = [
  new Date('25 December 2021'), // Chirstmas Holidays
  new Date('26 December 2021'),
  new Date('27 December 2021'),
  new Date('28 December 2021'),
  new Date('1 January 2022'), // New Years
  new Date('2 January 2022'),
  new Date('3 January 2022'),
  new Date('4 January 2022'),
  new Date('31 January 2022'), // Anniversary
  new Date('6 February 2022'), // Waitangi
  new Date('7 February 2022'),
  new Date('15 April 2022'), // Easter
  new Date('18 April 2022'),
  new Date('25 April 2022'), // Anzac
  new Date('24 June 2022'), // Matariki
  new Date('24 October 2022'), // Labour weekend
];
