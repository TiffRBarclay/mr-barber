export const SHOP_HOURS: {
  [shop: number]: { day: string; timeRange: string }[];
} = {
  1: [
    {
      day: 'Monday to Friday',
      timeRange: '9am - 6pm',
    },
    {
      day: 'Saturday',
      timeRange: '9am - 4pm',
    },
    {
      day: 'Sunday',
      timeRange: '9am - 3pm',
    },
  ],
  3: [
    {
      day: 'Saturday',
      timeRange: '9am - 4pm',
    },
  ],
};
