import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('Renders title', () => {
  render(<App />);
  expect(screen.getAllByText(/Mr\. BARBER/).length).toBeGreaterThan(0);
});
